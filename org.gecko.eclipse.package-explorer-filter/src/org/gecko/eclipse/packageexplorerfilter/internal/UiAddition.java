package org.gecko.eclipse.packageexplorerfilter.internal;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class UiAddition {
	
	private Text filterTextField;
	private Button showClosedButton;

	public void build(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).create());
		composite.setLayout(new GridLayout(2, false));

		filterTextField = new Text(composite, SWT.BORDER);
		filterTextField.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		showClosedButton = new Button(composite, SWT.CHECK);
		showClosedButton.setToolTipText("Show closed Projects");
		showClosedButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
	}

	public Text getFilterTextField() {
		return filterTextField;
	}

	public Button getShowClosedButton() {
		return showClosedButton;
	}

}
