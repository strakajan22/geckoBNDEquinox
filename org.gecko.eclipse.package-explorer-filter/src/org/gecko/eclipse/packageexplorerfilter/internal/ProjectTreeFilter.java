package org.gecko.eclipse.packageexplorerfilter.internal;

import org.eclipse.core.internal.resources.Project;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.ui.internal.WorkingSet;

public class ProjectTreeFilter extends ViewerFilter {

	private String filterText = "";
	private boolean showClosedProjects;

	public void setFilterText(String filterText) {
		this.filterText = filterText;
	}

	/**
	 * filter project names and hide workingsets if they don't contain visible
	 * elements
	 */
	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		if (element instanceof Project) {
			Project project = (Project) element;
			String elementName = project.getName();
			if (!showClosedProjects && !project.isOpen()) {
				return false;
			}
			return containsAnyPartOfFilterString(elementName);
		} else if (element instanceof JavaProject) {
			JavaProject javaProject = (JavaProject) element;
			if (!showClosedProjects && !javaProject.getProject().isOpen()) {
				return false;
			}
			String elementName = javaProject.getElementName();
			return containsAnyPartOfFilterString(elementName);
		} else if (element instanceof WorkingSet) {
			WorkingSet workingSet = ((WorkingSet) element);
			return hasVisibleElements(workingSet);
		}
		return true;
	}

	private boolean hasVisibleElements(WorkingSet workingSet) {
		for (IAdaptable iAdaptable : workingSet.getElements()) {
			Project project = iAdaptable.getAdapter(Project.class);
			if (project != null && select(null, null, project)) {
				return true;
			}
			JavaProject javaProject = iAdaptable.getAdapter(JavaProject.class);
			if (javaProject != null && select(null, null, javaProject)) {
				return true;
			}
		}
		return false;
	}

	private boolean containsAnyPartOfFilterString(String elementName) {
		if (elementName == null) {
			return true;
			// that seems to be unlikely but we don't wont to hide projects
			// without name
		}

		for (String filterTextElement : filterText.split("\\|")) {
			if (elementName.contains(filterTextElement)) {
				return true;
			}
		}
		return false;
	}

	public void setShowClosedProjects(boolean selection) {
		showClosedProjects = selection;
	}

}
