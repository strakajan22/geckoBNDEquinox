
package org.gecko.eclipse.packageexplorerfilter;

import java.lang.reflect.Field;
import java.util.Arrays;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.jdt.internal.ui.packageview.PackageExplorerPart;
import org.eclipse.jdt.internal.ui.workingsets.WorkingSetModel;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkingSet;
import org.eclipse.ui.internal.WorkbenchPartReference;
import org.eclipse.ui.internal.e4.compatibility.CompatibilityView;
import org.gecko.bnd.eclipse.rcp.annotations.RequireEclipseConsole;
import org.gecko.bnd.eclipse.rcp.annotations.RequireEclipseE4;
import org.gecko.bnd.eclipse.rcp.annotations.RequireEclipseEMF;
import org.gecko.bnd.eclipse.rcp.annotations.RequireEclipseIDE;
import org.gecko.bnd.eclipse.rcp.annotations.RequireEclipsePDE;
import org.gecko.eclipse.packageexplorerfilter.internal.ProjectTreeFilter;
import org.gecko.eclipse.packageexplorerfilter.internal.UiAddition;
import org.osgi.service.event.Event;

@RequireEclipseIDE
@RequireEclipseConsole
@RequireEclipseEMF
@RequireEclipseE4
@RequireEclipsePDE
public class FilterAddon {

	private static final String PACKAGE_EXPLORER = "org.eclipse.jdt.ui.PackageExplorer";

	private String lastUsedFilter;

	@Inject
	@Optional
	public void handleBringToTop(@UIEventTopic(UIEvents.UILifeCycle.BRINGTOTOP) Event event) {
		MPart mPart = (MPart) event.getProperty(UIEvents.EventTags.ELEMENT);
		if (!mPart.getElementId().equals(PACKAGE_EXPLORER)) {
			return;
		}
		
		WorkbenchPartReference reference = ((CompatibilityView) mPart.getObject()).getReference();
		PackageExplorerPart packageExplorerPart = null;
		try {
			Field f = WorkbenchPartReference.class.getDeclaredField("legacyPart");
			f.setAccessible(true);
			packageExplorerPart = (PackageExplorerPart) f.get(reference);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			throw new IllegalStateException("Could not access PackageExplorer contents", e);
		}

		final TreeViewer treeViewer = packageExplorerPart.getTreeViewer();

		for (ViewerFilter viewerFilter : Arrays.asList(treeViewer.getFilters())) {
			if (viewerFilter.getClass().equals(ProjectTreeFilter.class)) {
				return;
				// the package explorer was already upgraded
			}
		}

		Control control = treeViewer.getControl();
		control.setLayoutData(GridDataFactory.fillDefaults().grab(true, true).create());

		Composite parent = control.getParent();
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		parent.setLayout(layout);

		final ProjectTreeFilter treeFilter = new ProjectTreeFilter();
		treeViewer.addFilter(treeFilter);

		UiAddition uiAddition = new UiAddition();
		uiAddition.build(parent);

		final Text search = uiAddition.getFilterTextField();
		search.setMessage("Project Name Filter");
		search.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				treeFilter.setFilterText(search.getText());
				treeViewer.refresh();
				expandWorkingSets(treeViewer);
			}
		});

		parent.layout(true);

		search.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.ESC) {
					if (search.getText().isEmpty()) {
						search.setText(lastUsedFilter);
					} else {
						lastUsedFilter = search.getText();
						search.setText("");
					}
				}
			}
		});

		Button showClosedButton = uiAddition.getShowClosedButton();
		showClosedButton.addListener(SWT.Selection, e -> {
			treeFilter.setShowClosedProjects(showClosedButton.getSelection());
			treeViewer.refresh();
			expandWorkingSets(treeViewer);
		});

	}

	private void expandWorkingSets(TreeViewer treeViewer) {
		Object input = treeViewer.getInput();
		if (input instanceof WorkingSetModel) {
			WorkingSetModel model = (WorkingSetModel) input;
			IWorkingSet[] allWorkingSets = model.getAllWorkingSets();
			treeViewer.setExpandedElements(allWorkingSets);
		}
	}
}
