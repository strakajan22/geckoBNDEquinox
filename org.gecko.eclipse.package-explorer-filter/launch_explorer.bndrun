#-runfw: org.apache.felix.framework;version=5
#-runee: JavaSE-1.8
-runsystemcapabilities: ${native_capability}
-runprovidedcapabilities: ${native_capability}

-resolve.effective: active;skip:="osgi.service"

# the system package must have the verison, because there is the 
# org.apache.servicemix.specs.annotation-api-1.3 and javax.annotation bundle 
# that export it with the version 1.3, so some, bundles get wired to the system javax. annotation 
# packages and some to the ones from providing bundles. This is only necessary when running on java < 9
-runsystempackages: \
        javax.annotation;version=1.3

-runbundles: \
	com.ibm.icu;version='[64.2.0,64.2.1)',\
	javax.inject;version='[1.0.0,1.0.1)',\
	org.apache.batik.constants;version='[1.11.0,1.11.1)',\
	org.apache.batik.css;version='[1.11.0,1.11.1)',\
	org.apache.batik.i18n;version='[1.11.0,1.11.1)',\
	org.apache.batik.util;version='[1.11.0,1.11.1)',\
	org.apache.commons.io;version='[2.6.0,2.6.1)',\
	org.apache.commons.jxpath;version='[1.3.0,1.3.1)',\
	org.apache.commons.logging;version='[1.2.0,1.2.1)',\
	org.apache.felix.scr;version='[2.1.14,2.1.15)',\
	org.apache.xmlgraphics;version='[2.3.0,2.3.1)',\
	org.eclipse.compare;version='[3.7.700,3.7.701)',\
	org.eclipse.compare.core;version='[3.6.600,3.6.601)',\
	org.eclipse.core.commands;version='[3.9.500,3.9.501)',\
	org.eclipse.core.contenttype;version='[3.7.400,3.7.401)',\
	org.eclipse.core.databinding;version='[1.7.500,1.7.501)',\
	org.eclipse.core.databinding.observable;version='[1.8.0,1.8.1)',\
	org.eclipse.core.databinding.property;version='[1.7.100,1.7.101)',\
	org.eclipse.core.expressions;version='[3.6.500,3.6.501)',\
	org.eclipse.core.filebuffers;version='[3.6.700,3.6.701)',\
	org.eclipse.core.filesystem;version='[1.7.500,1.7.501)',\
	org.eclipse.core.resources;version='[3.13.500,3.13.501)',\
	org.eclipse.core.runtime;version='[3.16.0,3.16.1)',\
	org.eclipse.e4.core.commands;version='[0.12.700,0.12.701)',\
	org.eclipse.e4.core.contexts;version='[1.8.200,1.8.201)',\
	org.eclipse.e4.core.di;version='[1.7.400,1.7.401)',\
	org.eclipse.e4.core.di.annotations;version='[1.6.400,1.6.401)',\
	org.eclipse.e4.core.di.extensions;version='[0.15.300,0.15.301)',\
	org.eclipse.e4.core.di.extensions.supplier;version='[0.15.400,0.15.401)',\
	org.eclipse.e4.core.services;version='[2.2.0,2.2.1)',\
	org.eclipse.e4.emf.xpath;version='[0.2.400,0.2.401)',\
	org.eclipse.e4.ui.bindings;version='[0.12.600,0.12.601)',\
	org.eclipse.e4.ui.css.core;version='[0.12.800,0.12.801)',\
	org.eclipse.e4.ui.css.swt;version='[0.13.600,0.13.601)',\
	org.eclipse.e4.ui.css.swt.theme;version='[0.12.400,0.12.401)',\
	org.eclipse.e4.ui.di;version='[1.2.600,1.2.601)',\
	org.eclipse.e4.ui.dialogs;version='[1.1.600,1.1.601)',\
	org.eclipse.e4.ui.model.workbench;version='[2.1.500,2.1.501)',\
	org.eclipse.e4.ui.services;version='[1.3.600,1.3.601)',\
	org.eclipse.e4.ui.widgets;version='[1.2.500,1.2.501)',\
	org.eclipse.e4.ui.workbench;version='[1.10.100,1.10.101)',\
	org.eclipse.e4.ui.workbench.addons.swt;version='[1.3.600,1.3.601)',\
	org.eclipse.e4.ui.workbench.renderers.swt;version='[0.14.800,0.14.801)',\
	org.eclipse.e4.ui.workbench.swt;version='[0.14.700,0.14.701)',\
	org.eclipse.e4.ui.workbench3;version='[0.15.200,0.15.201)',\
	org.eclipse.emf.common;version='[2.16.0,2.16.1)',\
	org.eclipse.emf.ecore;version='[2.19.0,2.19.1)',\
	org.eclipse.emf.ecore.change;version='[2.14.0,2.14.1)',\
	org.eclipse.emf.ecore.xmi;version='[2.16.0,2.16.1)',\
	org.eclipse.equinox.bidi;version='[1.2.100,1.2.101)',\
	org.eclipse.equinox.common;version='[3.10.500,3.10.501)',\
	org.eclipse.equinox.p2.engine;version='[2.6.400,2.6.401)',\
	org.eclipse.equinox.preferences;version='[3.7.500,3.7.501)',\
	org.eclipse.equinox.registry;version='[3.8.500,3.8.501)',\
	org.eclipse.equinox.security;version='[1.3.300,1.3.301)',\
	org.eclipse.help;version='[3.8.500,3.8.501)',\
	org.eclipse.jface;version='[3.17.0,3.17.1)',\
	org.eclipse.jface.databinding;version='[1.9.100,1.9.101)',\
	org.eclipse.jface.text;version='[3.15.300,3.15.301)',\
	org.eclipse.ltk.core.refactoring;version='[3.10.200,3.10.201)',\
	org.eclipse.ltk.ui.refactoring;version='[3.10.0,3.10.1)',\
	org.eclipse.osgi.services;version='[3.8.0,3.8.1)',\
	org.eclipse.swt;version='[3.112.0,3.112.1)',\
	org.eclipse.team.core;version='[3.8.700,3.8.701)',\
	org.eclipse.team.ui;version='[3.8.600,3.8.601)',\
	org.eclipse.text;version='[3.9.0,3.9.1)',\
	org.eclipse.ui;version='[3.114.0,3.114.1)',\
	org.eclipse.ui.editors;version='[3.12.0,3.12.1)',\
	org.eclipse.ui.forms;version='[3.8.100,3.8.101)',\
	org.eclipse.ui.ide;version='[3.16.0,3.16.1)',\
	org.eclipse.ui.navigator;version='[3.9.0,3.9.1)',\
	org.eclipse.ui.navigator.resources;version='[3.7.0,3.7.1)',\
	org.eclipse.ui.views;version='[3.10.0,3.10.1)',\
	org.eclipse.ui.views.properties.tabbed;version='[3.8.600,3.8.601)',\
	org.eclipse.ui.workbench;version='[3.116.0,3.116.1)',\
	org.eclipse.ui.workbench.texteditor;version='[3.13.0,3.13.1)',\
	org.tukaani.xz;version='[1.8.0,1.8.1)',\
	org.w3c.css.sac;version='[1.3.1,1.3.2)',\
	org.w3c.dom.events;version='[3.0.0,3.0.1)',\
	org.w3c.dom.smil;version='[1.0.1,1.0.2)',\
	org.w3c.dom.svg;version='[1.1.0,1.1.1)',\
	org.eclipse.core.variables;version='[3.4.600,3.4.601)',\
	org.eclipse.ui.console;version='[3.8.600,3.8.601)',\
	org.eclipse.debug.core;version='[3.14.0,3.14.1)',\
	org.eclipse.debug.ui;version='[3.14.200,3.14.201)',\
	org.eclipse.jdt.core;version='[3.19.0,3.19.1)',\
	org.eclipse.jdt.core.manipulation;version='[1.12.100,1.12.101)',\
	org.eclipse.jdt.debug;version='[3.13.100,3.13.101)',\
	org.eclipse.jdt.launching;version='[3.15.0,3.15.1)',\
	org.eclipse.jdt.ui;version='[3.19.0,3.19.1)',\
	org.eclipse.search;version='[3.11.700,3.11.701)',\
	org.eclipse.equinox.p2.metadata.repository;version='[1.3.200,1.3.201)',\
	org.eclipse.core.jobs;version='[3.10.500,3.10.501)',\
	org.eclipse.equinox.app;version='[1.4.300,1.4.301)',\
	org.eclipse.equinox.p2.core;version='[2.6.100,2.6.101)',\
	org.eclipse.equinox.p2.metadata;version='[2.4.500,2.4.501)',\
	org.eclipse.equinox.p2.repository;version='[2.4.500,2.4.501)',\
	org.apache.felix.eventadmin;version='[1.5.0,1.5.1)',\
	org.eclipse.osgi.util;version='[3.5.300,3.5.301)',\
	org.gecko.eclipse.package-explorer-filter;version=snapshot
	

-runfw: org.eclipse.osgi;version='[3.15.0.v20190830-1434,3.15.0.v20190830-1434]'
-runee: JavaSE-11

-runrequires: bnd.identity;id='org.gecko.eclipse.package-explorer-filter'