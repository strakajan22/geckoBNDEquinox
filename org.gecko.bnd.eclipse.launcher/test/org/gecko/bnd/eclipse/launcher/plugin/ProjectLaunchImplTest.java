package org.gecko.bnd.eclipse.launcher.plugin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;

import aQute.bnd.build.ProjectLauncher;
import aQute.bnd.build.Run;
import aQute.bnd.build.Workspace;

public class ProjectLaunchImplTest {

	private Workspace	ws;
	private File		wsDir;

	File						base					= new File("").getAbsoluteFile();
	private File generatedDir;
	private File result;

//	@After
	public void after() {
		ws.close();
//		if(result != null) {
//			result.delete();
//		}
	}
	
//	@Before
	protected void setUp() throws Exception {
		wsDir = new File("../").getAbsoluteFile();
		System.out.println("Workspace dir " + wsDir.toString());
		if(wsDir.exists()) {
			System.out.println("Workspace dir " + wsDir.toString() + " exists");
		}
		ws = Workspace.getWorkspace(wsDir);
		generatedDir = new File(base, "generated/");
	}

	protected void tearDown() throws Exception {
		ws.close();
	}

//	@Test
	public void testPluginRun() throws Exception {
		setUp();
		File bndrun = new File("testresources/eclipseStyleTest.bndrun");
		
		assertTrue(bndrun.exists());
				
		Run run = Run.createRun(ws,bndrun);
		
		try {
			ProjectLauncher projectLauncher = run.getProjectLauncher();
			assertTrue(projectLauncher instanceof EclipseSytleProjectLauncherImpl);
			int result = projectLauncher.launch();
			projectLauncher.close();
			assertEquals(42, result);
		} finally {
			run.close();
			after();
		}
	}

//	public void testExport() throws Exception {
//		
//		File bndrun = new File("testresources/eclipseStyleTest.bndrun");
//		
//		assertTrue(bndrun.exists());
//		
//		Run run = Run.createRun(ws,bndrun);
//		
//		try {
//			ProjectLauncher projectLauncher = run.getProjectLauncher();
//			assertTrue(projectLauncher instanceof EclipseSytleProjectLauncherImpl);
//			Jar executable = projectLauncher.executable();
//			assertNotNull(executable);
//			
//			result = new File(generatedDir, "test.zip");
//			executable.write(result);
//			
//			assertTrue(result.exists());
//		} finally {
//			run.close();
//			ws.close();
//		}
//	}
}
