
package org.gecko.equinox.feature.converter;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.core.resources.IFile;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.gecko.bnd.eclipse.rcp.annotations.RequireEclipseConsole;
import org.gecko.bnd.eclipse.rcp.annotations.RequireEclipseE4;
import org.gecko.bnd.eclipse.rcp.annotations.RequireEclipseEMF;
import org.gecko.bnd.eclipse.rcp.annotations.RequireEclipseIDE;
import org.gecko.bnd.eclipse.rcp.annotations.RequireEclipsePDE;
import org.gecko.equinox.feature.converter.internal.FeatureRequirementCreator;
import org.gecko.equinox.feature.converter.internal.FeatureToTextConverter;

@RequireEclipseIDE
@RequireEclipseConsole
@RequireEclipseE4
@RequireEclipseEMF
@RequireEclipsePDE
public class FeatureConverterPart {
	private Text text;
	private IFile featureFile;
	private Text packageName;

	@Inject
	public FeatureConverterPart() {

	}

	@PostConstruct
	public void postConstruct(Composite parent) {
		Composite composite = new Composite(parent, 0);
		composite.setLayout(new GridLayout(2, false));


		Button button = new Button(composite, 0);
		button.setText("Create Requirement Files");
		button.addListener(SWT.Selection, e -> {
			FeatureRequirementCreator.fileToRequirementString(featureFile, packageName.getText());
		});
		button.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
	
		packageName = new Text(composite, SWT.BORDER);
		packageName.setMessage("enter package name here");
		packageName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		text = new Text(composite, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		text.setLayoutData(new GridData(GridData.FILL_BOTH));
		text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));

	}

	@Focus
	public void setFocus() {
		text.setFocus();

	}

	/**
	 * This method is kept for E3 compatiblity. You can remove it if you do not mix
	 * E3 and E4 code. <br/>
	 * With E4 code you will set directly the selection in ESelectionService and you
	 * do not receive a ISelection
	 * 
	 * @param s the selection received from JFace (E3 mode)
	 */
	@Inject
	@Optional
	public void setSelection(@Named(IServiceConstants.ACTIVE_SELECTION) ISelection s) {
		if (s == null || s.isEmpty())
			return;
		if (s instanceof IStructuredSelection) {
			IStructuredSelection iss = (IStructuredSelection) s;
			if (iss.size() == 1)
				setSelection(iss.getFirstElement());

		}
	}

	@Inject
	@Optional
	public void setSelection(@Named(IServiceConstants.ACTIVE_SELECTION) Object o) {
		// Remove the 2 following lines in pure E4 mode, keep them in mixed mode
		if (o instanceof ISelection) // Already captured
			return;

		if (o instanceof IFile) {
			IFile iFile = (IFile) o;
			if (iFile.getLocation().toFile().getName().endsWith("feature.xml")) {
				featureFile = iFile;
				String req = FeatureToTextConverter.fileToRequirementString(iFile);
				text.setText(req);
			}
		}

	}

}