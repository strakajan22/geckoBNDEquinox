package org.gecko.equinox.feature.converter.internal;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.pde.internal.core.FeatureModelManager;
import org.eclipse.pde.internal.core.PDECore;
import org.eclipse.pde.internal.core.ifeature.IFeature;
import org.eclipse.pde.internal.core.ifeature.IFeatureChild;
import org.eclipse.pde.internal.core.ifeature.IFeatureModel;

@SuppressWarnings("restriction")
public class FeatureRequirementCreator {

	public static void processRecursive(IFeature feature,File folder, String packageName) {
		RequirementJavaFile.create(feature, folder, packageName);
		IFeatureChild[] includedFeatures = feature.getIncludedFeatures();
		if (includedFeatures != null) {
			for (IFeatureChild includedFeature : includedFeatures) {
				FeatureModelManager manager = PDECore.getDefault().getFeatureModelManager();
				IFeatureModel findFeatureModel = manager.findFeatureModel(includedFeature.getId());
				if (findFeatureModel != null) {
					IFeature feature2 = findFeatureModel.getFeature();
					processRecursive(feature2, folder, packageName);
				} else {
					System.err.println(includedFeature.getId() + " could not be found");
				}
			}
		}
	}

	public static void fileToRequirementString(IFile featureFile, String packageName) {
		FeatureModelManager manager = PDECore.getDefault().getFeatureModelManager();
		IFeatureModel featureModel = manager.getFeatureModel(featureFile.getProject());
		IFeature feature = featureModel.getFeature();
		File folder = featureFile.getParent().getLocation().toFile();
		processRecursive(feature, folder, packageName);
	}

}
