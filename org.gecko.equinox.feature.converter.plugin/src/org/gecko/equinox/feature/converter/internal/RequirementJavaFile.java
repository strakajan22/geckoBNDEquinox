package org.gecko.equinox.feature.converter.internal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.pde.internal.core.ifeature.IFeature;
import org.eclipse.pde.internal.core.ifeature.IFeatureChild;
import org.eclipse.pde.internal.core.ifeature.IFeaturePlugin;

@SuppressWarnings("restriction")
public class RequirementJavaFile {

	private static final String EMPTY = "";
	private static final String EOL = "\r\n";

	public static void create(IFeature feature, File folder, String packageName) {

		List<String> lines = new ArrayList<>();

		lines.add("package " + packageName + ";");
		lines.add(EMPTY);
		lines.add("import java.lang.annotation.Documented;");
		lines.add("import java.lang.annotation.ElementType;");
		lines.add("import java.lang.annotation.Retention;");
		lines.add("import java.lang.annotation.RetentionPolicy;");
		lines.add("import java.lang.annotation.Target;");
		lines.add(EMPTY);
		lines.add("import org.osgi.annotation.bundle.Requirement;");
		lines.add(EMPTY);

		for (IFeaturePlugin plugin : feature.getPlugins()) {
			String optionalString = EMPTY;
			if (plugin.getOS() != null || plugin.getArch() != null) {
				optionalString = ", resolution = Resolution.OPTIONAL";
			}
			lines.add("@Requirement(namespace=\"osgi.identity\", name=\"" + plugin.getId() + "\" " + optionalString
					+ ")");
		}

		for (IFeatureChild iFeatureChild : feature.getIncludedFeatures()) {
			lines.add("@" + idToName(iFeatureChild.getId()));
		}

		lines.add("@Target(value={ElementType.PACKAGE, ElementType.TYPE})");
		lines.add("@Retention(value=RetentionPolicy.CLASS)");
		lines.add("@Documented");
		lines.add("public @interface " + idToName(feature.getId()) + " {");
		lines.add(EMPTY);
		lines.add("}");

		StringBuilder stringBuilder = new StringBuilder();
		for (String string : lines) {
			stringBuilder.append(string + EOL);
		}

		try (BufferedWriter writer = new BufferedWriter(
				new FileWriter(folder + "/" + idToName(feature.getId()) + ".java"));) {
			writer.write(stringBuilder.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static String idToName(String id) {
		String[] split = id.split("\\.");
		StringBuilder stringBuilder = new StringBuilder();
		for (String element : split) {
			stringBuilder.append(Character.toUpperCase(element.charAt(0)) + element.substring(1));
		}
		return "Require" + stringBuilder.toString();
	}

}
