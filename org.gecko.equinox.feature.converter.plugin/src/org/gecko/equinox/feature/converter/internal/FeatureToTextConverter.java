package org.gecko.equinox.feature.converter.internal;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.pde.internal.core.FeatureModelManager;
import org.eclipse.pde.internal.core.PDECore;
import org.eclipse.pde.internal.core.ifeature.IFeature;
import org.eclipse.pde.internal.core.ifeature.IFeatureChild;
import org.eclipse.pde.internal.core.ifeature.IFeatureModel;
import org.eclipse.pde.internal.core.ifeature.IFeaturePlugin;

@SuppressWarnings("restriction")
public class FeatureToTextConverter {

	public static void addFeatureToText(IFeature feature, List<String> list) {

		list.add(feature.getId());
		for (IFeaturePlugin iFeaturePlugin : feature.getPlugins()) {
			String id = iFeaturePlugin.getId();
			list.add("@Requirement(namespace=\"osgi.identity\", name=\"" + id + "\")");
		}
	}

	public static void processRecursive(IFeature feature, List<String> list) {

		addFeatureToText(feature, list);

		IFeatureChild[] includedFeatures = feature.getIncludedFeatures();
		if (includedFeatures != null) {
			for (IFeatureChild includedFeature : includedFeatures) {
				FeatureModelManager manager = PDECore.getDefault().getFeatureModelManager();
				IFeatureModel findFeatureModel = manager.findFeatureModel(includedFeature.getId());
				if (findFeatureModel != null) {
					IFeature feature2 = findFeatureModel.getFeature();
					processRecursive(feature2, list);
				} else {
					System.err.println(includedFeature.getId() + " could not be found");
				}
			}
		}
	}

	public static String fileToRequirementString(IFile featureFile) {
		FeatureModelManager manager = PDECore.getDefault().getFeatureModelManager();
		IFeatureModel featureModel = manager.getFeatureModel(featureFile.getProject());
		IFeature feature = featureModel.getFeature();

		List<String> list = new ArrayList<>();
		processRecursive(feature, list);

		StringBuilder stringBuilder = new StringBuilder();
		for (String string : list) {
			stringBuilder.append(string + "\n");
		}
		return stringBuilder.toString();
	}

}
