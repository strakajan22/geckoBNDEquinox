/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.eclipse.core.supplement.localization;

import java.util.ResourceBundle;

/**
 * Interface for a OSGI bundle based {@link ResourceBundle}
 * @author Mark Hoffmann
 * @since 07.11.2019
 */
public interface BundleResourceBundle {
	
	void setParent(ResourceBundle parent);

	boolean isEmpty();

	boolean isStemEmpty();
}
