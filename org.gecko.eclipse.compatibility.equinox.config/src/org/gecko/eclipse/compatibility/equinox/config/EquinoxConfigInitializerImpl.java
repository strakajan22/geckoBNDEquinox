package org.gecko.eclipse.compatibility.equinox.config;

import org.eclipse.osgi.internal.framework.EquinoxConfiguration;
import org.eclipse.osgi.service.environment.EnvironmentInfo;
import org.gecko.eclipse.compatibility.equinox.config.api.EquinoxConfigInitializer;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true)
public class EquinoxConfigInitializerImpl implements EquinoxConfigInitializer {

	@Reference
	private EnvironmentInfo envInfo;
	
	@Reference(target="(launcher.arguments=*)")
	ServiceReference<Object> launcher;
	
	public void activate() {
		System.out.println("Setting missing Equinox Arguments");
		EquinoxConfiguration equinoxConfig = (EquinoxConfiguration) envInfo;

		String[] args = (String[]) launcher.getProperty("launcher.arguments");
		
		equinoxConfig.setAllArgs(args);
		equinoxConfig.setFrameworkArgs(args);
		equinoxConfig.setAppArgs(args);
	}

}
